const express = require('express');
const app = express();
const exphbs = require('express-handlebars');
var RestClient = require('node-rest-client').Client;
const restClient = new RestClient();
const path = require('path');
var mysql = require('mysql2');
var fs = require('fs');
var SSHClient = require('ssh2').Client;

app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: path.join(__dirname, 'views/layouts')
}));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static("assets"));

var SQLconnection = null;
var localhost = '127.0.0.1';

var config = {};
config.db_user = 'root';
config.db_password = 'VKf123#!23'; //,
config.database = 'msxfcc';
config.userName = 'finance';
config.webHostRedisScript = '118.89.253.215';
config.redisFolder = 'redis';
config.rsa_key_path = 'C:/Users/mikhail-n/Documents/WeChat Files/vadim_bochenev/Files/id_rsa';
config.passphrase = "+52,p78VmLxX{U&Uh'etRp];6%credit";
config.sqlDBPort = '3307';
config.coreHost = '118.89.250.60';
config.skillsetNames = [];
config.webHost = '';
config.freeSwitches = [];
config.freeSwitchesIP = [];

console.log(process.argv);
process.argv.forEach(function (val, index, array) {
    let ind = val.indexOf('config=');
    if (ind !== -1) {
        config = JSON.parse(fs.readFileSync(val.substring(val.lastIndexOf('=') + 1)));
        config.freeSwitches = config.freeSwitches.sort();
        return;
    }

});

var RSA_KEY = fs.readFileSync(config.rsa_key_path);
var sshConnectionRedis = new SSHClient(),
    sshConnectionDialer = new SSHClient(),
    sshConnectionDB = new SSHClient(),
    sshConnectionSpark = new SSHClient(),
    sshConnectionDialerFSs = [],
    sshConnectionFSs = [],
    sshConnectionDialerFormulas = [],
    sshConnectionDialerTasks = [];
var skillSetCallTasksId = [];
var currentParameters = [];
var currentChartsData = [];
var currentRedisData = '';
var currentFsLoading = [];
var currentFsCritStates = [];
var currentFormulas = [];
var sparkError = false;

let connectionsStates = {
    redis: false,
    dialer: false,
    dialerFS: false,
    dbHost: false
};

config.freeSwitches.forEach(function (fs) {
    sshConnectionDialerFSs.push(new SSHClient());
    let connection = sshConnectionDialerFSs[sshConnectionDialerFSs.length - 1];

    connection.on('ready', function () {
        console.log(`SSH DIALER FS Client for ${fs}  :: ready`);
        connectionsStates.dialerFS = true;
    }).on('close', function () {
        console.log(`SSH DIALER FS ${fs} :: CLOSED`);
        connection.connect({
            host: config.coreHost,
            port: 22,
            username: config.userName,
            passphrase: config.passphrase,
            privateKey: RSA_KEY
        });
    }).connect({
        host: config.coreHost,
        port: 22,
        username: config.userName,
        passphrase: config.passphrase,
        privateKey: RSA_KEY
    });
});

config.freeSwitchesIP.forEach(function (fs) {
    sshConnectionFSs.push(new SSHClient());
    let connection = sshConnectionFSs[sshConnectionFSs.length - 1];

    connection.on('ready', function () {
        console.log(`SSH FS Client for ${fs}  :: ready`);
        connectionsStates.FSclients = true;
    }).on('close', function () {
        connection.connect({
            host: fs,
            port: 22,
            username: config.userName,
            passphrase: config.passphrase,
            privateKey: RSA_KEY
        });
    }).connect({
        host: fs,
        port: 22,
        username: config.userName,
        passphrase: config.passphrase,
        privateKey: RSA_KEY
    });
});

config.skillsetNames.forEach(function (skillName) {
    sshConnectionDialerFormulas.push(new SSHClient());
    let connection = sshConnectionDialerFormulas[sshConnectionDialerFormulas.length - 1];

    connection.on('ready', function () {
        console.log(`SSH DIALER FORMULA Client for ${skillName}  :: ready`);
        connectionsStates.dialerFS = true;
    }).on('close', function () {
        console.log(`SSH DIALER FORMULA ${skillName} :: CLOSED`);
        connection.connect({
            host: config.coreHost,
            port: 22,
            username: config.userName,
            passphrase: config.passphrase,
            privateKey: RSA_KEY
        });
    }).connect({
        host: config.coreHost,
        port: 22,
        username: config.userName,
        passphrase: config.passphrase,
        privateKey: RSA_KEY
    });
});

sshConnectionRedis.on('ready', function () {
    console.log('SSH Client redis :: ready');
    connectionsStates.redis = true;
}).on('close', function () {
    console.log('SSH Client redis :: CLOSED');
    sshConnectionRedis.end();
    throw 'SSH Client redis :: Connection lost. Try to reload';
}).connect({
    host: config.webHostRedisScript,
    port: 22,
    username: config.userName,
    passphrase: config.passphrase,
    privateKey: RSA_KEY
});

sshConnectionDialer.on('ready', function () {
    console.log('SSH DIALER Client :: ready');
    connectionsStates.dialer = true;
}).on('close', function () {
    console.log('SSH DIALER :: CLOSED');
    sshConnectionDialer.end();
    throw 'SSH DIALER Client :: Connection lost. Try to reload';
}).connect({
    host: config.coreHost,
    port: 22,
    username: config.userName,
    passphrase: config.passphrase,
    privateKey: RSA_KEY
});

sshConnectionDB.on('ready', function () {
    console.log('SSH Client DB:: ready');
    connectionsStates.dbHost = true;

    SQLconnection = mysql.createConnection({
        host: config.sqldb_internal_host,
        user: config.db_user,
        password: config.db_password,
        database: config.database,
        port: config.sqlDBPort
    });

    query = `SELECT id,name,maxLimitOfCalls,predictiveDialerCorrectionFactor FROM msxfcc.skillset WHERE name IN ('${config.skillsetNames.join("','")}')`;
    SQLconnection.query(query,
        function (error, results) {
            if (error) {
                console.error('GET FIRST TIME LIMITS :: sql error - ');
                console.error(error);
                throw 'SQLconnection DB :: Try to reload';
            }
            currentParameters = results;

            let date = new Date();
            date.setHours(0, 0, 0);
            query = `SELECT skillset, id  FROM msxfcc.call_task where  start_time > ${date.getTime()}`;
            console.log('select tasks id');
            SQLconnection.query(query,
                function (error, res) {
                    if (error) {
                        console.error('GET TASK IDs :: sql error - ');
                        console.error(error);
                        return;
                    }
                    connectionsStates.tasksFound = true;

                    console.log(res);


                    res.forEach(function (task) {
                        let ind = currentParameters.findIndex(function (skill) {
                            return task.skillset == skill.id
                        });
                        if (ind !== -1) skillSetCallTasksId.push({
                            skillset: currentParameters[ind].name,
                            taskId: task.id,
                            isWorking: true
                        });
                    });

                    skillSetCallTasksId.forEach(function (task) {
                        sshConnectionDialerTasks.push(new SSHClient());
                        let connection = sshConnectionDialerTasks[sshConnectionDialerTasks.length - 1];

                        connection.on('ready', function () {
                            console.log(`SSH DIALER TASK Client for ${task.skillset} task - ${task.taskId}  :: ready`);
                            connectionsStates.dialerTasks = true;
                        }).on('close', function () {
                            console.log(`SSH DIALER TASK Client for ${task.skillset} task - ${task.taskId} :: CLOSED`);
                            //  connection.end();
                            //  throw `SSH DIALER TASK Client for ${task.skillset} task - ${task.taskId} :: Connection lost. Try to reload`;
                        }).connect({
                            host: config.coreHost,
                            port: 22,
                            username: config.userName,
                            passphrase: config.passphrase,
                            privateKey: RSA_KEY
                        });
                    });

                });
        });

    setInterval(function () {
        query = `SELECT name,maxLimitOfCalls,predictiveDialerCorrectionFactor FROM msxfcc.skillset WHERE name IN ('${config.skillsetNames.join("','")}')`;
        SQLconnection.query(query,
            function (error, results) {
                if (error) {
                    console.error('GET LIMITS :: sql error - ');
                    console.error(error);
                    return;
                }
                currentParameters = results;
            });
    }, 30 * 1000);
}).on('close', function () {
    console.log('SSH Client DB :: CLOSED');
    sshConnectionDB.end();
    throw 'SSH Client DB :: Connection lost. Try to reload';
}).connect({
    host: config.webHostRedisScript,
    port: 22,
    username: config.userName,
    passphrase: config.passphrase,
    privateKey: RSA_KEY
});


sshConnectionSpark.on('ready', function () {
    console.log('SSH Client DB:: ready');
    connectionsStates.spark = true;


}).on('close', function () {
    console.log('SSH Client DB :: CLOSED');
    sshConnectionSpark.connect({
        host: config.coreHost,
        port: 22,
        username: config.userName,
        passphrase: config.passphrase,
        privateKey: RSA_KEY
    });
}).connect({
    host: config.coreHost,
    port: 22,
    username: config.userName,
    passphrase: config.passphrase,
    privateKey: RSA_KEY
});

let waitStart = setInterval(function () {
    if (connectionsStates.redis === true &&
        connectionsStates.dialer === true &&
        connectionsStates.dbHost === true &&
        connectionsStates.dialerTasks &&
        connectionsStates.dialerFS &&
        connectionsStates.FSclients &&
        connectionsStates.tasksFound) {

        clearInterval(waitStart);

        setInterval(function () {
            let results = {skillsets: []};
            config.skillsetNames.forEach(function (skillName) {
                if (!restClient.methods[skillName]) {
                    restClient.registerMethod(skillName, `https://${config.webHost}//public/api/statistic/tasksQuantity?bucketId=${skillName}`, "GET");
                }
                restClient.methods[skillName](function (data) {
                    results.skillsets.push({
                        name: skillName,
                        onCalls: data.onCalls,
                        acw: data.acw,
                        ready: data.ready,
                        notReady: data.notReady,
                        totalAbandon: data.totalAbandon,
                        totalConnect: data.onCalls + data.acw + data.ready + data.notReady
                    })
                });

            });
            let counter = 0;
            let un = setInterval(function () {
                counter++;
                if (counter > 30) {
                    console.log('/getData timeout');
                    clearInterval(un);
                    return;
                }
                if (results.skillsets.length === config.skillsetNames.length) {
                    clearInterval(un);
                    //console.log('GET CHARTS DATA END')
                    currentChartsData = results.skillsets.sort(function (item1, item2) {
                        return item1.name > item2.name
                    });
                }
            }, 300);

        }, 1500);

        setInterval(function () {
            currentRedisData = '';
            sshConnectionRedis.exec(`cd ${config.redisFolder} ; sh redis_count_monitor.sh`, function (err, stream) {
                if (err) {
                    console.log('SSH REDIS ::ERR: ' + err);
                    return;
                }
                if (stream) {
                    stream.on('close', function () {
                        // console.log('GET REDIS DATA END');
                    }).on('data', function (data, stderr) {
                        if (stderr) {
                            console.log('SSH REDIS STDERR: ' + data);
                        }
                        else {
                            currentRedisData += data.toString('utf8');
                        }
                    })
                }
            });
        }, 1500);

            sshConnectionDialerFSs.forEach(function (dialerConnection, index) {
                dialerConnection.exec(`tail -n 1 -f --pid=${process.pid} ${config.logPaths.dialersManager} | grep -m 1 ${config.freeSwitches[index]}`, function (err, stream) {
                    if (err) {
                        console.log('GET FS DATA EXEC ERR: ' + err);
                        dialerConnection.connect({
                            host: config.coreHost,
                            port: 22,
                            username: config.userName,
                            passphrase: config.passphrase,
                            privateKey: RSA_KEY
                        });
                        return;
                    }
                    if (stream) {
                        stream.on('close', function () {
                            //console.log('GET fs data DATA END  ' + currentFsLoading);
                        }).on('data', function (data, stderr) {
                            if (stderr) {
                                console.log(`/getFSData STDERR for ${config.freeSwitches[index]} : ` + stderr);
                                return;
                            }
                            currentFsLoading[index] = data.toString('utf8');
                        });
                    }
                });
            });

        setInterval(function () {
            sshConnectionFSs.forEach(function (connection, index) {

                let dataSelect = '';
                connection.exec(`tail -n 500 ${config.logPaths.freeswitch} | grep -e "\\[CRIT\\]" -e "NATIVE SQL ERR"`, function (err, stream) {
                    if (err) {
                        console.log(`GET FS CRIT ERROR EXEC ERR ${config.freeSwitches[index]}: ` + err);
                        connection.connect({
                            host: config.freeSwitchesIP[index],
                            port: 22,
                            username: config.userName,
                            passphrase: config.passphrase,
                            privateKey: RSA_KEY
                        });
                        return;
                    }
                    if (stream) {
                        stream.on('close', function () {
                            if (dataSelect)
                                currentFsCritStates[index] = {state: true, value: dataSelect};
                            else
                                currentFsCritStates[index] = {state: false, value: dataSelect};
                        }).on('data', function (data, stderr) {
                            if (stderr) {
                                console.log(`GET FS CRIT STDERR for ${config.freeSwitches[index]} : ` + stderr);
                                return;
                            }
                            dataSelect += data.toString('utf8');
                        });
                    }
                });
            });
        }, 3 * 1000);


            sshConnectionDialerFormulas.forEach(function (connection, index) {
                let skillName = config.skillsetNames[index];
                connection.exec(`tail -n 1 -f --pid=${process.pid} ${config.logPaths.dialersManager} | grep -m 1 Skillset=${skillName}`, function (err, stream) {
                    if (err) {
                        console.log(`SSH DIALER FORMULA ${skillName} EXEC ERR: ` + err);
                        sshConnectionDialer.connect({
                            host: config.coreHost,
                            port: 22,
                            username: config.userName,
                            passphrase: config.passphrase,
                            privateKey: RSA_KEY
                        });
                        return;
                    }
                    if (stream) {
                        stream.on('close', function () {
                            // console.log('GET dialer FORMULA END');
                        }).on('data', function (data, stderr) {
                            if (stderr) {
                                console.log(` SSH DIALER FORMULA ${skillname} STDERR: ` + data);
                                return;
                            }
                            let ind = currentFormulas.findIndex(function (item) {
                                return item.name === skillName;
                            });
                            if (ind !== -1) {
                                currentFormulas[ind] = {name: skillName, log: data.toString('utf8')};
                            } else {
                                currentFormulas.push({name: skillName, log: data.toString('utf8')});
                            }
                        })
                    } else {
                        console.log(`SSH DIALER FORMULA ${skillName} no stream`);
                    }
                });
            });


        // check tasks logs
        setInterval(function () {
            skillSetCallTasksId.forEach(function (skillTask, index, array) {
                let connection = sshConnectionDialerTasks[index];
                let dataSelect = '';
                connection.exec(`tail -n 1500 ${config.logPaths.dialersManager} | grep -m 1 "callTaskId = ${skillTask.taskId}"`, function (err, stream) {
                    if (err) {
                        console.log(`SSH DIALER TASKS  ${skillTask.skillset} task - ${skillTask.taskId} EXEC ERR: ` + err);
                        connection.connect({
                            host: config.coreHost,
                            port: 22,
                            username: config.userName,
                            passphrase: config.passphrase,
                            privateKey: RSA_KEY
                        });
                        return;
                    }
                    if (stream) {
                        stream.on('close', function () {
                            if (!dataSelect)
                                array[index].isWorking = false;
                            else
                                array[index].isWorking = true;
                        }).on('data', function (data, stderr) {
                            if (stderr) {
                                console.log(`SSH DIALER TASKS  ${skillTask.skillset} task - ${skillTask.taskId} STDERR: ` + data);
                                return;
                            }
                            dataSelect = data;
                        })
                    } else {
                        console.log(`SSH DIALER TASK ${skillTask.skillset} task - ${skillTask.taskId}  no stream`);
                    }
                });
            });
        }, 5 * 1000);

        listenSparkLogs();

        function listenSparkLogs() {
                sshConnectionSpark.exec(`tail -n 1 -f --pid=${process.pid} ${config.logPaths.spark} | grep -m 1 Exception`, function (err, stream) {
                    if (err) {
                        console.log(`SSH SPARK CONNECTION EXEC ERR: ` + err);
                        connection.on('ready', function () {
                            console.log('SSH SPARK CONNECTION reconnected');
                            setTimeout(function () {
                                listenSparkLogs();
                            }, 1500)
                        }).connect({
                            host: config.coreHost,
                            port: 22,
                            username: config.userName,
                            passphrase: config.passphrase,
                            privateKey: RSA_KEY
                        });
                        return;
                    }
                    if (stream) {
                        stream.on('close', function () {

                        }).on('data', function (data, stderr) {
                            if (stderr) {
                                console.log(`SSH SPARK CONNECTION STDERR: ` + data);
                                return;
                            }
                            sparkError = data.toString('utf8');
                        })
                    } else {
                        console.log(`SSH SPARK CONNECTION no stream`);
                    }
                });

        };

    }
}, 1000);


// =================================================================== //
app.get('/', function (request, responseHtml) {
    let taskResult = false;

    let results = {skillsets: [], tasks: []};
    config.skillsetNames.forEach(function (skillName) {
        if (!restClient.methods[skillName]) {
            restClient.registerMethod(skillName, `https://${config.webHost}//public/api/statistic/tasksQuantity?bucketId=${skillName}`, "GET");
        }
        restClient.methods[skillName](function (data) {
            results.skillsets.push({
                name: skillName,
                onCalls: data.onCalls,
                acw: data.acw,
                ready: data.ready,
                notReady: data.notReady,
                totalAbandon: data.totalAbandon,
                totalConnect: data.onCalls + data.acw + data.ready + data.notReady
            })
        });
    });

    let counter = 0;
    let un = setInterval(function () {
        counter++;

        if (counter > 40) {
            clearInterval(un);
            console.log('/ timeout');
            responseHtml.render('monitoringPage', {});
            return;
        }
        if (results.skillsets.length === config.skillsetNames.length) {
            clearInterval(un);

            results.skillsets = results.skillsets.sort(function (item1, item2) {
                return item1.name > item2.name
            });
            results.skillsets = JSON.stringify(results.skillsets);
            responseHtml.render('monitoringPage', results);
        }
    }, 300);
});

app.get('/getRedisData', function (request, res) {
    res.json(currentRedisData);
});

app.get('/isSparkAlive', function(req, res) {
    let host = req.query.sparkHost;
    let cl = restClient.get(host, function (data, response) {
        res.json('true');
    });
    cl.on('error', function () {
        res.json('false');
    });
});

app.get('/getFormulaData', function (request, res) {
    let results = {
        formulas: currentFormulas.sort(function (item1, item2) {
            return item1.name > item2.name
        }).map(function (item) {
            return item.log;
        })
    };
    res.json(results);
});

app.get('/getFSData', function (request, res) {
    res.json({loading: currentFsLoading, states: currentFsCritStates});
});

app.get('/getData', function (request, res) {
    res.json(currentChartsData);
});

app.get('/getSparkData', function (request, res) {
    res.json(sparkError);
});

app.get('/getSQLData', function (request, res) {
    skillSetCallTasksId.forEach(function (task) {
        let ind = currentParameters.findIndex(function (param) {
            return param.name == task.skillset
        });
        if (ind !== -1) currentParameters[ind].isWorking = task.isWorking;

    });
    res.json(currentParameters.sort(function (item1, item2) {
        return item1.name > item2.name;
    }));
});

app.post('/stopCM', function (request, res) {

    let index = request.query.number;
    console.log('stop CM invoked for ' + (index + 1) + ' callManager');

    sshConnectionDialerFSs[index].exec('cdconf; cd call-manager; ./remove.sh', function (err, stream) {
        if (err) {
            console.log(`SSH STOP CALL_MANAGER ${index + 1} EXEC ERR: `+ err);
            res.end('false');
            return;
        }
        if (stream) {
            stream.on('close', function () {
                res.end('true');
            }).on('data', function (data) {
            })
        } else {
            console.log(`SSH STOP CALL_MANAGER ${index + 1} no stream`);
        }
    });


});


app.post('/restartSpark', function (request, res) {
    console.log('restart Spark invoked');
    sshConnectionSpark.exec(`cdconf; cd spark; ./stop.sh; ./start.sh`, function (err, stream) {
        if (err) {
            console.log(`SSH SPARK RESTART EXEC ERR: ` + err);
            res.end('false');
            return;
        }
        if (stream) {
            stream.on('close', function () {
                res.end('true');
            }).on('data', function (data) {
            })
        } else {
            console.log(`SSH SPARK RESTART no stream`);
        }
    });
});
app.post('/updateDataBase', function (request, res) {
    console.log('updateDataBase');
    console.log(request.query);
    setMaxLimit(request.query.mlc, request.query.param, request.query.skillname).then(function (result) {
        res.writeHead(200, {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers": "Content-Type,Access-Control-Allow-Headers, Authorization, X-Requested-With",
            "Access-Control-Allow-Methods": "GET, POST, OPTIONS",
            "Access-Control-Allow-Origin": "*"
        });
        res.end(result);
    }, function (error) {
        res.writeHead(500);
        res.end(JSON.stringify(error));
    });
});

app.post('/resetToZero', function (request, res) {
    console.log('updateDataBase');
    console.log(request.query);
    setZeroLimit().then(function (result) {
        res.writeHead(200, {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers": "Content-Type,Access-Control-Allow-Headers, Authorization, X-Requested-With",
            "Access-Control-Allow-Methods": "GET, POST, OPTIONS",
            "Access-Control-Allow-Origin": "*"
        });
        res.end(result);
    }, function (error) {
        res.writeHead(500);
        res.end(JSON.stringify(error));
    });
});

app.post('/restartServer', function (request, res) {
        sshConnectionRedis.end();
        sshConnectionDialer.end();
        sshConnectionDB.end();
        sshConnectionSpark.end();
        sshConnectionDialerFSs.forEach(function(item){item.end()});
        sshConnectionFSs.forEach(function(item){item.end()});
        sshConnectionDialerFormulas.forEach(function(item){item.end()});
        sshConnectionDialerTasks.forEach(function(item){item.end()});
    throw 'Restart command received';
});

function setMaxLimit(maxLimitCalls, parameter, skillName) {
    return new Promise(function (resolve, reject) {
        SQLconnection.query(`UPDATE msxfcc.skillset SET maxLimitOfCalls='${maxLimitCalls}',predictiveDialerCorrectionFactor = '${parameter}'  WHERE name='${skillName}'`,
            function (error, results, fields) {
                if (error) {
                    console.error('UPDATE LIMIT ::  sql error - ');
                    console.error(error);
                    reject(error);
                } else {
                    if (results.changedRows === 1) {
                        resolve('<strong style="color: green">OK</strong>');
                    } else {
                        resolve('<strong style="color: red">FAIL</strong>');
                    }
                }
            });
    })
}

function setZeroLimit() {
    return new Promise(function (resolve, reject) {
        SQLconnection.query(`UPDATE msxfcc.skillset SET maxLimitOfCalls=0 WHERE name IN ('${config.skillsetNames.join("','")}')`,
            function (error, results, fields) {
                if (error) {
                    console.error('UPDATE LIMIT ::  sql error - ');
                    console.error(error);
                    reject(error);
                } else {
                    if (results.changedRows >= 1) {
                        resolve('<strong style="color: green">OK</strong>');
                    } else {
                        resolve('<strong style="color: red">FAIL</strong>');
                    }
                }
            });
    })
}

app.listen(config.port, function (err) {
    if (err) {
        console.log('something bad happened', err);
        return
    }
    console.log(`server is listening on ${config.port}`)
})
