from general_coniguration import *


class Server:
    def __init__(self, json_object):
        self.server = {'hostname': json_object['Hostname'],
                       'group': json_object['Group'],
                       'core_manager': json_object['Core Services'],
                       'dialers_manager': json_object['Dialers Manager'],
                       'call_manager': json_object['Call Manager'],
                       'rts': json_object['RTS Manager'],
                       'call_recorder': json_object['Call Recorder'],
                       'web_user_portal': json_object['WEB User Portal'],
                       'web_public_api': json_object['WEB Public API']}

    def print_server_config(self):
        print("Hostname: " + str(self.server['hostname']))
        print("   Core Services: " + str(self.server['core_manager']))
        print("   Dialers Manager: " + str(self.server['dialers_manager']))
        print("   Call Manager: " + str(self.server['call_manager']))
        print("   RTS Manager: " + str(self.server['rts']))
        print("   Call Recorder: " + str(self.server['call_recorder']))
        print("   WEB User Portal: " + str(self.server['web_user_portal']))
        print("   WEB Public API: " + str(self.server['web_public_api']))

    def append_tree(self, tree):
        BUILD_TARGET = get_build_target()
        parent = tree.insert(parent='',
                             index='end',
                             iid=str(self.server['hostname']),
                             text=self.server['hostname'],
                             value=self.server['group'])
        for app_name in self.server.keys():
            if app_name != 'hostname' and app_name != 'group':
                iid = str(self.server['hostname']) + '.' + app_name

                child = tree.insert(parent,
                                    index='end',
                                    iid=iid,
                                    text=BUILD_TARGET[app_name]['NAME'],
                                    value='' if app_name == 'call_manager' else self.server[app_name])
                # I used global variable to save server-iid pairs
                # it is easiest way to retrieve server by iid
                append_servers_dict(self, iid)

                if app_name == 'call_manager':
                    items = self.server[app_name]
                    for item in range(len(items)):
                        for tag in BUILD_TARGET[app_name]['tags']:
                            cm_iid = iid + str(self.server[app_name][item]['componentId']) + '_' + tag + '_' + \
                                     str(self.server[app_name][item][tag])
                            tree.insert(child,
                                        index='end',
                                        iid=cm_iid,
                                        text=tag,
                                        value=self.server[app_name][item][tag])
                            # I used global variable to save server-iid pairs
                            # it is easiest way to retrieve server by iid
                            append_servers_dict(self, iid)

    @staticmethod
    def server_creator(input_json):
        d = json.loads(input_json)
        current_server = None
        try:
            current_server = Server(d)
        except KeyError:
            print(d)

        return current_server

    @staticmethod
    def get_server_by_iid(iid):
        return get_servers_dict()[iid]

    @staticmethod
    def get_list_of_servers(filename):
        list_of_server = []

        with open(filename, 'r') as fp:
            try:
                obj = json.load(fp)
                number_of_servers = len(obj["configs"])
                for i in range(number_of_servers):
                    dummy_json = json.dumps(obj["configs"][i])
                    current_server = Server.server_creator(dummy_json)
                    if current_server is not None:
                        list_of_server.append(current_server)

            except ValueError:
                print("error loading JSON")

        return list_of_server
