from tkinter import ttk, BooleanVar
from server_configuration import *
import tkinter.filedialog as filedialog
from build_commands import *
from connector import *
import time
import threading
from Std_redirector import *
import sys

IntOptions = {
    'age': (1.0, 200.0, 1.0),
}


class App(object):

    def __init__(self, master):
        # Upload DB config from db.json
        setUP()
        self.master = master
        self.master.title("Configuration")
        self.master.columnconfigure(0, weight=1)
        self.master.rowconfigure(0, weight=1)
        self.use_http = BooleanVar()
        self.print_only = BooleanVar()
        self.build_require = BooleanVar()
        self.skip_tests = BooleanVar()
        self.create_btn_frame()
        self.create_check_box_frame()
        self.tree = self.create_tree_frame()
        self.list_of_servers = []

    def create_btn_frame(self):
        # Frame for button
        panelFrame = tk.Frame(self.master)
        panelFrame.grid(row=1, column=0, sticky=tk.NSEW)
        loadBtn = tk.Button(panelFrame, text='Load', command=self.load_json)
        submitBtn = tk.Button(panelFrame, text='Submit', command=self.submit_changes)
        printBtn = tk.Button(panelFrame, text='Print', command=self.print_current)

        loadBtn.grid(row=0, column=0)
        submitBtn.grid(row=0, column=1)
        printBtn.grid(row=0, column=2)
        printBtn.bind('<Return>', self.print_current)
        loadBtn.bind('<Return>', self.load_json)

    def create_check_box_frame(self):
        panelFrame = tk.Frame(self.master)
        panelFrame.grid(row=2, column=0, sticky=tk.NSEW)
        chk_test_only = tk.Checkbutton(panelFrame, text="Print Only", variable=self.print_only)
        chk_test_only.grid(row=0, column=0)

        chk_use_http = tk.Checkbutton(panelFrame, text="Use HTTP", variable=self.use_http)
        chk_use_http.grid(row=0, column=1)

        chk_build_require = tk.Checkbutton(panelFrame, text="Build Project", variable=self.build_require)
        chk_build_require.grid(row=0, column=2)

        chk_skip_tests = tk.Checkbutton(panelFrame, text="Skip Tests", variable=self.skip_tests)
        chk_skip_tests.grid(row=0, column=3)

    def submit_changes(self):
        """""
         1. Build project
         2. Upload built application through docker build or scp
         3. Perform ./remove and ./deploy scripts on the remote site
        """""
        set_print_only(self.print_only.get())
        set_use_http(self.use_http.get())
        set_global_build_require(self.build_require.get())
        set_global_skip_tests(self.skip_tests.get())

        new_window = tk.Toplevel(self.master)
        text = tk.Text(new_window)
        text.config(width=200, height=50)
        text.pack()
        sys.stdout = StdRedirector(text)

        set_global_text_box(text)

        thread1 = threading.Thread(target=self.deploy)
        thread1.start()

    def deploy(self):
        if perform_project_build() == 0:
            backup_pom_files()
            for index in range(len(self.list_of_servers)):
                server = self.list_of_servers[index].server
                print("Perform upload for: " + server['hostname'])
                perform_upload_to_server(server)
                perform_deploy_on_remote_site(server)

            restore_pom_files()

        print("DONE!")
        return SUCCESS

    def print_current(self):
        for i in range(len(self.list_of_servers)):
            self.list_of_servers[i].print_server_config()

    def load_json(self):
        self.list_of_servers = []
        fn = filedialog.Open(self.master).show()
        if fn == '':
            return ERROR
        self.list_of_servers = Server.get_list_of_servers(fn)
        for i in range(len(self.list_of_servers)):
            self.list_of_servers[i].append_tree(self.tree)

    def update_servers(self, value, iid):
        server = Server.get_server_by_iid(iid)
        application_name = iid.split('.')[-1]
        print(application_name)
        server.server[application_name] = value

    def create_tree_frame(self):
        TreeFrame = ttk.Frame(self.master, padding="3")
        TreeFrame.grid(row=0, column=0, sticky=tk.NSEW)
        tree = ttk.Treeview(TreeFrame, columns='Values')
        tree.column('Values', width=100, anchor='center')
        tree.heading('Values', text='Values')
        tree.pack(fill=tk.BOTH, expand=1)
        return tree
