import inspect
import xml.etree.ElementTree as ET
import subprocess
from connector import *
from pathlib import Path
import os
if get_global_use_gui():
    from tkinter import *


if os.name == 'nt':
    COPY='copy'
else:
    COPY='cp'


def __FUNCTION__():
    return inspect.stack()[1][3]


def perform_project_build():

    if not get_global_build_require():
        print("Build procedure has been skipped")
        return SUCCESS

    PROJECT_DIR = get_project_dir()

    print(__FUNCTION__())
    if get_print_only():
        return SUCCESS
    else:
        option=''
        if get_global_skip_tests():
            option = '-DskipTests=true'

        oldpath = os.getcwd()
        cmd = 'cd ' + str(Path(PROJECT_DIR)) + ' && ' + ' mvn clean && mvn install ' + option + ' && cd ' + oldpath

        perform_local_command(cmd)
        return SUCCESS


def perform_upload_to_server(server):
    BUILD_TARGET = get_build_target()
    PROJECT_DIR = get_project_dir()

    status = ERROR
    for app_name in server.keys():
        if app_name == 'hostname' or app_name == 'group':
            continue
        if app_name != 'call_manager' and server[app_name]:
            if BUILD_TARGET[app_name]['DOCKER_BUILD'] == 1:
                status = change_pom_file(server, app_name)
                if status == SUCCESS:
                    status = perform_maven_docker_build(app_name)
            else:
                src = str(Path(PROJECT_DIR + BUILD_TARGET[app_name]['TARGET']))
                dst = BUILD_TARGET[app_name]['TARGET_ON_SITE']
                print('send_file_to_server: ' + src + ' to ' + dst)
                if not get_print_only():
                    send_file_to_server(server, src, dst)
        elif app_name == 'call_manager':
            # we know that it requires docker build, then skip this validation
            # if one of instances has to be installed, perform docker build
            for item in range(len(server['call_manager'])):
                instance = server['call_manager'][item]
                if instance['install']:
                    status = perform_maven_docker_build(app_name)
                    break

    return status


def perform_maven_docker_build(app_name):
    BUILD_TARGET = get_build_target()
    PROJECT_DIR = get_project_dir()

    print(__FUNCTION__() + " CMD: mvn docker build from: " + str(Path(PROJECT_DIR + BUILD_TARGET[app_name]['TARGET'])))

    oldpath = os.getcwd()
    cmd = 'cd ' + str(Path(PROJECT_DIR + BUILD_TARGET[app_name]['TARGET'])) + ' && mvn docker:build && cd ' + oldpath

    print(__FUNCTION__() + ' ' + cmd)
    if not get_print_only():
        perform_local_command(cmd)
    return SUCCESS


def change_pom_file(server, app_name):
    """""
    register_namespace has to be set using configuration from pom.xml
    hope, that all pom.xml files have the same namespaces of XML
    """""
    BUILD_TARGET = get_build_target()
    PROJECT_DIR = get_project_dir()

    ET.register_namespace('', 'http://maven.apache.org/POM/4.0.0')
    ET.register_namespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance')
    filename = PROJECT_DIR + BUILD_TARGET[app_name]['TARGET'] + "pom.xml"

    tree = ET.parse(filename)
    root = tree.getroot()
    if change_docker_host(root, server, '{http://maven.apache.org/POM/4.0.0}dockerHost') == ERROR:
        print('Something wrong with docker host')
        return ERROR
    if change_docker_certs(root, '{http://maven.apache.org/POM/4.0.0}dockerCertPath', server) == ERROR:
        print('Something wrong with docker certs')
        return ERROR

    tree.write(filename, encoding="UTF-8", xml_declaration=True, method='xml')
    return SUCCESS


def change_docker_host(root, server, text):

    if root.find(text) is None:
        status = change_docker_host(root, server, '*/' + text)
    else:
        if get_use_http():
            print('Change Docker Host ' + root.find(text).text + ' to: http://' + str(server['hostname']) + ':2375')
            root.find(text).text = 'http://' + str(server['hostname']) + ':2375'
        else:
            print('Change Docker Host ' + root.find(text).text + ' to: https://' + str(server['hostname']) + ':2376')
            root.find(text).text = 'https://' + str(server['hostname']) + ':2376'

        status = SUCCESS

    return status


def change_docker_certs(root, text, server):

    CONFIG = get_system_config()
    if root.find(text) is None:
        status = change_docker_certs(root, '*/' + text, server)
    else:
        print('Change Docker Certs ' + root.find(text).text + ' to: ' + CONFIG[server['group']]['CERT_PATH'])
        root.find(text).text = CONFIG[server['group']]['CERT_PATH']
        status = SUCCESS

    return status


def perform_deploy_on_remote_site(server):
    BUILD_TARGET = get_build_target()

    for app_name in server.keys():
        if app_name == 'hostname' or app_name == 'group' or \
                        app_name == 'web_user_portal':
            continue

        if app_name != 'call_manager' and server[app_name] and \
                BUILD_TARGET[app_name]['DEPLOY_DIR'] != '':
            deploy_single_instance(server, app_name)
        elif app_name == 'call_manager':
            number_of_instances = len(server[app_name])
            # we know that it requires docker build, then skip this validation
            # if one of instances has to be installed, perform docker build
            for item in range(number_of_instances):
                instance = server[app_name][item]
                if instance['install']:
                    if number_of_instances == 1:
                        deploy_single_instance(server, app_name)
                    else:
                        deploy_single_instance(server, app_name, instance['componentId'])


def deploy_single_instance(server, app_name, instance_name=''):
    BUILD_TARGET = get_build_target()

    if instance_name == '':
        remove = BUILD_TARGET[app_name]['DEPLOY_DIR'] + '/remove.sh'
        deploy = BUILD_TARGET[app_name]['DEPLOY_DIR'] + '/deploy.sh'
    else:
        add_new_config_directory_for_application(server, app_name, instance_name)
        remove = BUILD_TARGET[app_name]['DEPLOY_DIR'] + '-' + instance_name + '/' + \
            'remove.sh ' + instance_name
        deploy = BUILD_TARGET[app_name]['DEPLOY_DIR'] + '-' + instance_name + '/' + \
            'deploy.sh ' + instance_name

    print("Perform remove on server: " + server['hostname'] + " : " + remove)
    if not get_print_only():
        send_command_to_server_by_key(server, remove)
    print("Perform deploy on server: " + server['hostname'] + " : " + deploy)
    if not get_print_only():
        send_command_to_server_by_key(server, deploy)
    return


def add_new_config_directory_for_application(server, app_name, instance_name):
    """""
    Need to copy /etc/cm (e.g.) to /etc/conf/cm-$instance_name
    based on config dir in general project
    """""
    BUILD_TARGET = get_build_target()

    new_config_dir = BUILD_TARGET[app_name]['CONF_PATH'] + '-' + instance_name
    # this command will be used on the remote Linux server, so definitely 'cp' is correct
    cmd = 'cp -ar ' + BUILD_TARGET[app_name]['CONF_PATH'] + ' ' + new_config_dir
    print(__FUNCTION__() + ": " + cmd)
    if not get_print_only():
        send_command_to_server_by_key(server, cmd)
    return SUCCESS


def backup_pom_files():
    BUILD_TARGET = get_build_target()
    PROJECT_DIR = get_project_dir()

    for item in BUILD_TARGET.keys():
        if BUILD_TARGET[item]['DOCKER_BUILD']:
            filename = PROJECT_DIR + BUILD_TARGET[item]['TARGET'] + "pom.xml"
            backup_filename = filename + ".backup"
            cmd = COPY + ' ' + str(Path(filename)) + ' ' + str(Path(backup_filename))
            print(__FUNCTION__() + ' ' + cmd)
            perform_local_command(cmd)

    return SUCCESS


def restore_pom_files():
    BUILD_TARGET = get_build_target()
    PROJECT_DIR = get_project_dir()

    for item in BUILD_TARGET.keys():
        if BUILD_TARGET[item]['DOCKER_BUILD']:
            filename = PROJECT_DIR + BUILD_TARGET[item]['TARGET'] + "pom.xml"
            backup_filename = filename + ".backup"
            cmd = COPY + ' ' + str(Path(backup_filename)) + ' ' + str(Path(filename))
            print(__FUNCTION__() + ' ' + cmd)
            perform_local_command(cmd)
    return SUCCESS


def perform_local_command(cmd):
    if get_global_use_gui():
        t = get_global_text_box()
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, bufsize=1, shell=True)
        for line in iter(p.stdout.readline, b''):
            t.insert(END, '%s\n' % line.rstrip().decode('utf-8'))
            t.see(END)
            t.update_idletasks()
        p.stdout.close()
        p.wait()
    else:
        os.system(cmd)

# perform_project_build()
# perform_upload_to_server('CALL_MANAGER')
# perform_upload_to_server('RTS_MANAGER')
