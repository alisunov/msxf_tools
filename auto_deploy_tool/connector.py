import paramiko
from general_coniguration import *
from scp import SCPClient
import os
from pathlib import Path
DEVNULL = open(os.devnull, 'wb')


def send_command_to_server_by_password(server, command):
    CONFIG = get_system_config()
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(hostname=server['hostname'],
                       username=CONFIG[server['group']]['SSH_USER'],
                       password=CONFIG[server['group']]['SSH_PASSWORD'],
                       port=CONFIG[server['group']]['SSH_REMOTE_PORT'])
        stdin, stdout, stderr = client.exec_command(command)
        data = stdout.read() + stderr.read()
        print(data)
    except paramiko.ssh_exception.AuthenticationException:
        print("incorrect password")

    client.close()


def send_command_to_server_by_key(server, command):
    CONFIG = get_system_config()
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(hostname=server['hostname'],
                       username=CONFIG[server['group']]['SSH_USER'],
                       password=CONFIG[server['group']]['SSH_PASSWORD'],
                       port=CONFIG[server['group']]['SSH_REMOTE_PORT'],
                       key_filename=str(Path(CONFIG[server['group']]['SSH_AUTH_KEY_FILE'])))
        stdin, stdout, stderr = client.exec_command(command)
        data = stdout.read() + stderr.read()
        print(data)
    except paramiko.ssh_exception.AuthenticationException:
        print("incorrect password")

    client.close()


def send_file_to_server(server, src, dst):
    CONFIG = get_system_config()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname=server['hostname'],
                    username=CONFIG[server['group']]['SSH_USER'],
                    password=CONFIG[server['group']]['SSH_PASSWORD'],
                    port=CONFIG[server['group']]['SSH_REMOTE_PORT'],
                    key_filename=str(Path(CONFIG[server['group']]['SSH_AUTH_KEY_FILE'])))
        with SCPClient(ssh.get_transport()) as scp:
            scp.put(src, dst)

    except paramiko.ssh_exception.AuthenticationException:
        print("send_file_to_server: incorrect password")

    ssh.close()
