import general_coniguration
general_coniguration.set_global_use_gui()
from App import *


def main():

    # Setup the root UI
    root = tk.Tk()
    app = App(root)

    # Limit windows minimum dimensions
    root.update_idletasks()
    root.minsize(root.winfo_reqwidth(), root.winfo_reqheight())
    root.mainloop()

if __name__ == "__main__":
    main()
