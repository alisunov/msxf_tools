import argparse
from server_configuration import *
from build_commands import *
from connector import *
import time

"""""
This script is the main for console version of autodeploy tool.
"""""

list_of_servers = []


def main():

    setUP()

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--conf", dest="filename", required=True,
                        help="Configuration of the server(s)", metavar="CONFIG_FILE")

    parser.add_argument("-t", "--test",
                        action="store_true", default=False, required=False,
                        help="If it is set, deploy will not be performed." \
                             "Even if this options is used, changing *.pom files are performed")

    parser.add_argument("--http", dest="use_http",
                        action="store_true", default=False, required=False,
                        help="If host does not support https connection for Docker images \n "
                             "use \'--http\' option to use HTTP(port 2375) instead of HTTPS (port 2376)")

    parser.add_argument("-b", "--build", dest="build_require",
                        action="store_true", default=False, required=False,
                        help="Defines whether maven build should be run or not")

    parser.add_argument("-s", "--skip", dest="skip_test",
                        action="store_true", default=False, required=False,
                        help="If it uses, -DskipTests=true option will be used")

    args = parser.parse_args()
    if args.test:
        set_print_only(True)
    else:
        set_print_only(False)

    if args.use_http:
        set_use_http(True)
    else:
        set_use_http(False)

    if args.build_require:
        set_global_build_require(True)
    else:
        set_global_build_require(False)

    if args.skip_test:
        set_global_skip_tests(True)
    else:
        set_global_skip_tests(False)

    print(args.filename)

    load_server_config(args.filename)
    print_current()
    submit_changes()


def load_server_config(filename):
    global list_of_servers
    if filename == '':
        return ERROR
    list_of_servers = Server.get_list_of_servers(filename)


def print_current():
    global list_of_servers
    for i in range(len(list_of_servers)):
        list_of_servers[i].print_server_config()


def submit_changes():
    """""
     1. Build project
     2. Upload built application through docker build or scp
     3. Perform ./remove and ./deploy scripts on the remote site
    """""

    if perform_project_build() == 0:
        backup_pom_files()
        for index in range(len(list_of_servers)):
            server = list_of_servers[index].server
            print("Perform upload for: " + server['hostname'])
            perform_upload_to_server(server)
            time.sleep(1)
            perform_deploy_on_remote_site(server)
            time.sleep(1)

        restore_pom_files()

    print("DONE!")
    return SUCCESS


if __name__ == "__main__":
    main()
