import tkinter as tk


class StdRedirector(object):
    def __init__(self,widget):
        self.widget = widget

    def write(self,string):
            self.widget.insert(tk.END,string)
            self.widget.see(tk.END)

    def flush(self):
        pass
