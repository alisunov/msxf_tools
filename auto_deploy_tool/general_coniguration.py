import json

ERROR = -1
SUCCESS = 0


"""
:var _servers_dict_: This dictionary contains pairs of complete iid and associated server object.
                     Then it is easy to get server object by any iid value. It is used for changing value 
                     in tkinter tree view for server's configuration
"""

_servers_dict_ = {}


def append_servers_dict(server, iid):
    global _servers_dict_
    _servers_dict_[iid] = server


def get_servers_dict():
    global _servers_dict_
    return _servers_dict_

"""
:var _print_only_: used for testing purposes. Even if this options is used, mvn build and changing *.pom files
                   are performed
"""

_print_only_ = True


def set_print_only(value):
    global _print_only_
    _print_only_ = value


def get_print_only():
    return _print_only_

_CONFIG_ = {}
_BUILD_TARGET_ = {}
_PROJECT_DIR_ = ''


def setUP():
    global _PROJECT_DIR_, _BUILD_TARGET_, _CONFIG_
    _DB_CONFIG_FILE_ = 'db.json'

    with open(_DB_CONFIG_FILE_, 'r') as fp:
        try:
            json_object = json.load(fp)
            _PROJECT_DIR_ = json_object['PROJECT_DIR']
            _BUILD_TARGET_ = json_object['BUILD_TARGET']
            _CONFIG_ = json_object['CONFIG']

        except KeyError:
            print("Incorrect db.json file")


def get_project_dir():
    global _PROJECT_DIR_
    return _PROJECT_DIR_


def get_build_target():
    global _BUILD_TARGET_
    return _BUILD_TARGET_


def get_system_config():
    global _CONFIG_
    return _CONFIG_

"""
:var _use_http_: If host with Docker is not configured for HTTPS connect for mvn docker:build
                 this variable has to be used to select HTTP connection instead of HTTPS.
                 It leads to changing *.pom file with http://hostname:2375 in <docker_host> line
"""
_use_http_ = False


def set_use_http(value):
    global _use_http_
    _use_http_ = value


def get_use_http():
    global _use_http_
    return _use_http_

"""
:var _text_: Used for saving tkinter Text object, which is used for
             redirecting stdout
"""
_text_ = None


def set_global_text_box(text):
    global _text_
    _text_ = text


def get_global_text_box():
    global _text_
    return _text_

"""
:var _use_gui_: Used for importing tkinter module
                It is used for redirecting stdout to separate window
           
"""
_use_gui_ = False


def set_global_use_gui():
    global _use_gui_
    _use_gui_ = True


def get_global_use_gui():
    global _use_gui_
    return _use_gui_

"""
:var _build_require_: Defines whether maven build should be run or not
"""

_build_require_ = False


def set_global_build_require(value):
    global _build_require_
    _build_require_ = value


def get_global_build_require():
    global _build_require_
    return _build_require_

"""
:var _build_require_: Defines whether maven build should be run or not
"""

_skip_tests_ = False


def set_global_skip_tests(value):
    global _skip_tests_
    _skip_tests_ = value


def get_global_skip_tests():
    global _skip_tests_
    return _skip_tests_
