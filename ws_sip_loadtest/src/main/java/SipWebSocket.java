import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@WebSocket
public class SipWebSocket {
    private int id;
    private CountDownLatch closeCountDownLatch = new CountDownLatch(1);
    private CountDownLatch connectCountDownLatch = new CountDownLatch(1);

    private Session session = null;

    public SipWebSocket(int id) {
        this.id = id;
    }

    public void awaitTermitaion(long timeout, TimeUnit timeUnit) throws InterruptedException {
        closeCountDownLatch.await(timeout, timeUnit);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Socket " + id + ": closed. Status code: " + statusCode + ". Reason: " + reason);
        closeCountDownLatch.countDown();
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.println("Socket " + id + " opened");
        this.session = session;
        connectCountDownLatch.countDown();
    }

    public Session awaitConnection(long timeout, TimeUnit timeUnit) throws InterruptedException {
        connectCountDownLatch.await(timeout, timeUnit);
        return session;
    }

    public void close() {
        session.close();
    }

    @OnWebSocketMessage
    public void onMessage(String msg) {
    }
}
