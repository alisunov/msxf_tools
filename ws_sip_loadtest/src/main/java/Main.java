import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    public static final String REGISTER_MESSAGE = "REGISTER sip:ss2.biloxi.example.com SIP/2.0\n" +
            "Via: SIP/2.0/WSS client.biloxi.example.com:5061;branch=z9hG4bKnashds7\n" +
            "Max-Forwards: 70\n" +
            "From: Bob <sip:bob@biloxi.example.com>;tag=a73kszlfl\n" +
            "To: Bob <sip:bob@biloxi.example.com>\n" +
            "Call-ID: 1j9FpLxk3uxtm8tn@biloxi.example.com\n" +
            "CSeq: 1 REGISTER\n" +
            "Contact: <sip:bob@client.biloxi.example.com>\n" +
            "Content-Length: 0\n\n";

    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {

        if (args.length < 3) {
            System.out.println("Args: <uri> <connection_limit> <thread_limit>");
            return;
        }

        String uri = args[0];
        int connectionLimit = Integer.parseInt(args[1]);
        int threadLimit = Integer.parseInt(args[2]);

        WebSocketClient webSocketClient = new WebSocketClient();
        try {
            webSocketClient.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", Integer.toString(threadLimit));
        Stream<Session> sessionStream = IntStream.range(0,connectionLimit).parallel()
                .mapToObj(id -> {
                    SipWebSocket sipWebSocket = new SipWebSocket(id);
                    try {
                        System.out.println("Connecting " + id);
                        webSocketClient.connect(sipWebSocket, new URI(uri), new ClientUpgradeRequest());
                        return sipWebSocket;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .map(socket -> {
                    try {
                        return socket.awaitConnection(5, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .map(session -> {
                    try {
//                        for (int i = 0; i < 100; i++) {
                            session.getRemote().sendString(REGISTER_MESSAGE);
                            session.getRemote().flush();
//                        }
                        return session;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                });
        List<Session> sessionList = sessionStream.collect(Collectors.toList());
        System.out.println("Threads connected: " + sessionList.size());
        Thread.sleep(5000);
        System.out.println("Closing");
        int closedNum = sessionList.stream().filter(Objects::nonNull)
                .mapToInt(s -> {
                    try {
                        if (ThreadLocalRandom.current().nextBoolean()) {
                            s.getRemote().sendString(REGISTER_MESSAGE);
                            s.getRemote().flush();
                        }
                        s.disconnect();
                        return 1;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return 0;
                    }
                }).sum();
        System.out.println("Successfully Closed " + closedNum);
        try {
            webSocketClient.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
