package com.company;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.ToLongFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class Main {

    public static final String TOTAL_EVENTS_STRING = "(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s\\[([\\w-]*)].*MQ: Received new event: (\\w*)";
    public static final String TRANSITION_STRING = "(\\d\\d:\\d\\d:\\d\\d\\.\\d\\d\\d).*Agent FSM \\((\\d*):(\\w*)\\): from (([\\w\\s]*) to ([\\w\\s]*) by (\\w*))";

    public static final String AGENT_D_LOCKED = "(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s\\[([\\w-]*)].*Agent (\\d*) in redis locked";
    public static final String AGENT_D_LOCKING = "(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s\\[([\\w-]*)].*Locking agent (\\d*)...";
    public static final String AGENT_D_UNLOCKED = "(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s\\[([\\w-]*)].*Agent (\\d*) in redis unlocked";

    public static final String CONTACT_D_LOCKED = "(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s\\[([\\w-]*)].*Contact (([0-9]|[a-z]|-)*) in redis locked";
    public static final String CONTACT_D_LOCKING = "(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s\\[([\\w-]*)].*Locking contact (([0-9]|[a-z]|-)*)...";
    public static final String CONTACT_D_UNLOCKED = "(\\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s\\[([\\w-]*)].*Contact (([0-9]|[a-z]|-)*) in redis unlocked";

    public static final String FILES_PATH = "D:\\__0";
    public static final String DATE_STAMP = "HH:mm:ss.SSS";

    private static class AgentData {
        public String id;
        public String name;
        public String startTime;
        public String endTime;

        @Override
        public String toString() {
            return "AgentData{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", startTime='" + startTime + '\'' +
                    ", endTime='" + endTime + '\'' +
                    '}';
        }
    }

    private static class MQEventsCalcData {
        public Long totalEventCount = 0L;
        public HashMap<String, Long> eventCountByType = new HashMap<>();
        public HashMap<String, Long> eventCountByThread = new HashMap<>();
    }

    private static class AgentLockingData {
        public Long lockingTime = -1L;
        public Long lockedTime = -1L;
        public Long unlockedTime = -1L;

        public Long lockedTimeInterval = -1L;
        public Long lockingTimeInterval = -1L;
        public Long lockInterval = -1L;

        public String thread = "";
    }


        public static void main(String[] args) {
//        collectTransitions();
//        findAgentsLocks();
        findContactsLocks();
//        calculateEventCount();
    }

    public static void calculateEventCount() {
        HashMap<Long, MQEventsCalcData> eventsCalcDataHashMap = new HashMap<>();
        MQEventsCalcData overallCalcData = new MQEventsCalcData();
        final Pattern eventsCalcPattern = Pattern.compile(TOTAL_EVENTS_STRING);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_STAMP);

        handleFiles((line) -> {
                    if (!line.contains("MQ: Received new event:")) {
                        return;
                    }
                    Matcher transitionMatcher = eventsCalcPattern.matcher(line);
                    while (transitionMatcher.find()) {
                        String timeString = transitionMatcher.group(1);
                        String thread = transitionMatcher.group(2);
                        String event = transitionMatcher.group(3);

                        overallCalcData.totalEventCount++;
                        overallCalcData.eventCountByThread.compute(thread, Main::initializeOrIncrementValue);
                        overallCalcData.eventCountByType.compute(event, Main::initializeOrIncrementValue);
                        try {
                            eventsCalcDataHashMap.compute(sdf.parse(timeString).toInstant().truncatedTo(ChronoUnit.MINUTES).toEpochMilli(),
                                    (k, v) -> {
                                        MQEventsCalcData mqEventsCalcData = v;
                                        if (v == null) {
                                            mqEventsCalcData = new MQEventsCalcData();
                                        }
                                        mqEventsCalcData.totalEventCount++;
                                        mqEventsCalcData.eventCountByThread.compute(thread, Main::initializeOrIncrementValue);
                                        mqEventsCalcData.eventCountByType.compute(event, Main::initializeOrIncrementValue);
                                        return mqEventsCalcData;
                                    });
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                },
                (f) -> {
                    System.out.print("\n\n\n");
                    System.out.println(f.getName());
                }
                , null);

        eventsCalcDataHashMap.entrySet().stream().sorted(Comparator.comparingLong(Map.Entry::getKey)).forEach((e) -> {
                    System.out.println(sdf.format(new Date(e.getKey())));
                    printEventCalcData(e.getValue());
                }
        );

        System.out.println("\n\n\nOverall:\n\n");
        printEventCalcData(overallCalcData);
    }

    public static void printEventCalcData(MQEventsCalcData data) {
        System.out.println("Total events: " + data.totalEventCount);
        System.out.println("\nEvents by type:");
        data.eventCountByType.forEach((e, c) -> System.out.println(e + ". " + c));
        System.out.println("\nEvents by thread:");
        data.eventCountByThread.forEach((e, c) -> System.out.println(e + ". " + c));
    }

    public static Long initializeOrIncrementValue(String key, Long value) {
        if (value == null) {
            return 1L;
        }
        return value + 1;
    }

    public static void findContactsLocks() {
        findLocks(CONTACT_D_LOCKED, CONTACT_D_LOCKING, CONTACT_D_UNLOCKED);
    }

    public static void findAgentsLocks() {
        findLocks(AGENT_D_LOCKED, AGENT_D_LOCKING, AGENT_D_UNLOCKED);
    }

    public static void collectTransitions() {
        Table<String, Long, AgentData> transitionTable = HashBasedTable.create();
        HashMap<String, Long> lastTransitionTime = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_STAMP);

        final Pattern transitionLine = Pattern.compile(TRANSITION_STRING);
        handleFiles((line) -> {
                    if (!line.contains("AgentCallFSMManager - Agent FSM")) {
                        return;
                    }
                    Matcher transitionMatcher = transitionLine.matcher(line);
                    while (transitionMatcher.find()) {
                        String id = transitionMatcher.group(2);
                        String name = transitionMatcher.group(3);
                        String srcdstEvent = transitionMatcher.group(4);
                        String sourceState = transitionMatcher.group(5);
                        String destState = transitionMatcher.group(6);
                        String event = transitionMatcher.group(7);
                        String timeString = transitionMatcher.group(1);
                        Long lastTime = lastTransitionTime.get(id);
                        AgentData agentData = new AgentData();
                        agentData.name = name;
                        agentData.id = id;
                        agentData.endTime = timeString;
                        agentData.startTime = lastTime == null ? "" : sdf.format(new Date(lastTime));
                        try {
                            Long time = sdf.parse(timeString).toInstant().toEpochMilli();
                            transitionTable.put(srcdstEvent, lastTime == null ? 0 : time - lastTime, agentData);
                            lastTransitionTime.put(id, time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                },
                (f) -> {
                    System.out.print("\n\n\n");
                    System.out.println(f.getName());
                }
                ,
                (f) -> {
                    printTransition(transitionTable, 10);
                }
        );
        System.out.println("\n\n\n\nResults:\n\n\n");
        printTransition(transitionTable, 100);
    }

    private static void printTransition(Table<String, Long, AgentData> transitionTable, Integer limit) {
        transitionTable.rowMap().forEach((k, v) -> {
            System.out.println("Transition: " + k);
            v.entrySet().stream().sorted(Comparator.comparingLong((ToLongFunction<Map.Entry<Long, AgentData>>) Map.Entry::getKey).reversed()).limit(limit)
                    .forEach(e -> {
                        System.out.println(e.getValue() + ". " + e.getKey());
                    });
            System.out.print("\n\n\n");
        });
    }

    public static void findLocks(String lockedString, String lockingString, String unlockedString) {
        Map<String, AgentLockingData> agentLockingDataHashMap = new HashMap<>();
        HashMap<String, Long> people = new HashMap<>();
        Pattern locked = Pattern.compile(lockedString);
        Pattern unlocked = Pattern.compile(unlockedString);
        Pattern locking = Pattern.compile(lockingString);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_STAMP);

        Map<String, AgentLockingData> finalAgentLockingDataHashMap = agentLockingDataHashMap;
        handleFiles((line) -> {
                    if (!line.contains("locked") && !line.contains("Locking")) {
                        return;
                    }
                    Matcher lockedMatcher = locked.matcher(line);
                    while (lockedMatcher.find()) {
                        String id = lockedMatcher.group(3);
                        String timeString = lockedMatcher.group(1);
                        people.compute(id, Main::initializeOrIncrementValue);
                        finalAgentLockingDataHashMap.compute(id, (k, v) -> {
                            if (v == null) {
                                return null;
                            }
                            try {
                                v.lockedTime = sdf.parse(timeString).toInstant().toEpochMilli();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            return v;
                        });
                    }

                    Matcher unlockedMatcher = unlocked.matcher(line);
                    while (unlockedMatcher.find()) {
                        String id = unlockedMatcher.group(3);
                        String timeString = unlockedMatcher.group(1);
                        people.compute(id, (k, v) -> {
                            if (v != null) {
                                return v > 0 ? v - 1 : 0;
                            }
                            else {
                                return 1L;
                            }
                        });
                        finalAgentLockingDataHashMap.compute(id, (k, v) -> {
                            if (v == null) {
                                return null;
                            }
                            try {
                                v.unlockedTime = sdf.parse(timeString).toInstant().toEpochMilli();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            return v;
                        });
                    }

                    Matcher lockingMatcher = locking.matcher(line);
                    while (lockingMatcher.find()) {
                        String id = lockingMatcher.group(3);
                        String timeString = lockingMatcher.group(1);
                        String thread = lockingMatcher.group(2);
                        finalAgentLockingDataHashMap.compute(id, (k, v) -> {
                            AgentLockingData ald = v;
                            if (v == null) {
                                ald = new AgentLockingData();
                                ald.thread = thread;
                            }
                            try {
                                ald.lockingTime = sdf.parse(timeString).toInstant().toEpochMilli();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            return ald;
                        });
                    }
                },
                (f) -> System.out.println("\n\n\n" + f.getName()),
                (f) -> printPeople(people)
        );
        System.out.println("Locked at the end:");
        printPeople(people);
        System.out.println("\n\n\nAvg data:");
        agentLockingDataHashMap = agentLockingDataHashMap.entrySet().stream()
                .filter(e -> e.getValue().lockedTime > 0 && e.getValue().lockingTime > 0 && e.getValue().unlockedTime > 0 && e.getValue().lockedTime >= e.getValue().lockingTime
                    && e.getValue().unlockedTime >= e.getValue().lockedTime
                )
                .peek(e -> {
                    e.getValue().lockingTimeInterval = e.getValue().lockedTime - e.getValue().lockingTime;
                    e.getValue().lockedTimeInterval = e.getValue().unlockedTime - e.getValue().lockedTime;
                    e.getValue().lockInterval = e.getValue().unlockedTime - e.getValue().lockingTime;
                }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        System.out.println("\n\n\nTop 100 locking interval items:");
        agentLockingDataHashMap.entrySet().stream().sorted(Comparator.comparingLong((ToLongFunction<Map.Entry<String, AgentLockingData>>)e -> e.getValue().lockingTimeInterval).reversed())
                .limit(100)
                .forEach(e -> System.out.println("Item " + e.getKey() + ": " + e.getValue().lockingTimeInterval + " (" + sdf.format(e.getValue().lockingTime) + ")" + " [" + e.getValue().thread + "]"));
        System.out.println("\n\n\nTop 100 locked interval items:");
        agentLockingDataHashMap.entrySet().stream().sorted(Comparator.comparingLong((ToLongFunction<Map.Entry<String, AgentLockingData>>)e -> e.getValue().lockedTimeInterval).reversed())
                .limit(100)
                .forEach(e -> System.out.println("Item " + e.getKey() + ": " + e.getValue().lockedTimeInterval + " (" + sdf.format(e.getValue().lockingTime) + ")" + " [" + e.getValue().thread + "]"));
        System.out.println("\n\n\nTop 100 full lock interval items:");
        agentLockingDataHashMap.entrySet().stream().sorted(Comparator.comparingLong((ToLongFunction<Map.Entry<String, AgentLockingData>>)e -> e.getValue().lockInterval).reversed())
                .limit(100)
                .forEach(e -> System.out.println("Item " + e.getKey() + ": " + e.getValue().lockInterval + " (" + sdf.format(e.getValue().lockingTime) + ")" + " [" + e.getValue().thread + "]"));
    }

    private static void handleFiles(Consumer<String> lineScanner, Consumer<File> sofHandler, Consumer<File> eofHandler) {
        File file = new File(FILES_PATH);
        File[] files = file.listFiles();
        if (files == null) {
            System.out.println("No files");
            return;
        }
        Arrays.sort(files);
        Arrays.stream(files)
//                .sorted(Comparator.comparingLong(File::lastModified))
                .forEach(f -> {
                    if (sofHandler != null) {
                        sofHandler.accept(f);
                    }
                    Scanner scanner;
                    try {
                        scanner = new Scanner(f);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        if (eofHandler != null) {
                            eofHandler.accept(f);
                        }
                        return;
                    }
                    while (scanner.hasNextLine()) {
                        lineScanner.accept(scanner.nextLine());
                    }
                    if (eofHandler != null) {
                        eofHandler.accept(f);
                    }
                });

    }

    private static void handleFiles(Consumer<String> lineScanner) {
        handleFiles(lineScanner, null, null);
    }

    private static void printPeople(HashMap<String, Long> agents) {
        System.out.println("\n\nLocked: \n");
        agents.entrySet().stream().filter(e -> e.getValue() > 0)
                .forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));
    }
}
